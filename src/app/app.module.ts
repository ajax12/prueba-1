import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';


import { APP_ROUTING } from './app.routes';

//services
import { ProductoService } from './services/producto.service';

import { AppComponent } from './app.component';
import { ProductosComponent } from './componentes/productos/productos.component';
import { CategoriasComponent } from './componentes/categorias/categorias.component';
import { NavBarComponent } from './componentes/nav-bar/nav-bar.component';
import { CategoriaMenuComponent } from './componentes/categoria-menu/categoria-menu.component';
import { SearchComponent } from './componentes/search/search.component';
import { NewsletterComponent } from './componentes/newsletter/newsletter.component';
import { SkillsComponent } from './componentes/skills/skills.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { PartnersComponent } from './componentes/partners/partners.component';
import { DetalleProductoComponent } from './componentes/detalle-producto/detalle-producto.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductosComponent,
    CategoriasComponent,
    NavBarComponent,
    CategoriaMenuComponent,
    SearchComponent,
    NewsletterComponent,
    SkillsComponent,
    FooterComponent,
    PartnersComponent,
    DetalleProductoComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    APP_ROUTING
  ],
  providers: [ProductoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
