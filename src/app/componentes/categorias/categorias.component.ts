import { Component, OnInit } from '@angular/core';
import { Categoria } from 'src/app/mis-clases/categoria';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {


  categorias: Categoria[];

  constructor(private productoService:ProductoService) { }

  ngOnInit(): void {

    this.productosPorCategoria();
  }
  productosPorCategoria() {

    this.productoService.getProductosPorCategoria().subscribe(
      data => {
        console.log("categorias producto" + JSON.stringify(data));
        this.categorias = data;
      }
    );
   
  }

}
