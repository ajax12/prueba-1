import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Producto } from 'src/app/mis-clases/producto';
import { ProductoService } from 'src/app/services/producto.service';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  styleUrls: ['./productos.component.css']
})
export class ProductosComponent implements OnInit {

  productos:Producto[];
  iDCategoriaActual: number;
  porBusqueda:boolean;

  constructor(private productoService: ProductoService,
    private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      
      this.listarProductos();
    });
  }

  listarProductos() {
    this.porBusqueda = this.route.snapshot.paramMap.has('clave');
    if(this.porBusqueda){
      this.cargarListaPorBusqueda();
    }else{
      this.cargaListaPorcategoria();
    }
   
  }

  cargaListaPorcategoria(){
     // verificar si el id esta disponible
     const hasCategoriaActualId = this.route.snapshot.paramMap.has('id');

     if(hasCategoriaActualId){
       // Obtener el parametro string id y convertir a number con "+"
       this.iDCategoriaActual = +this.route.snapshot.paramMap.get('id');
     }else{
       // La categoria no esta disponible, asignar valor por default
       this.iDCategoriaActual = 1;
     }
 // Haora obten los productos dando el id de la categoria
     this.productoService.getListarProductos(this.iDCategoriaActual)
       .subscribe((data) => (this.productos = data));

  }

  cargarListaPorBusqueda(){

    const miClave:string = this.route.snapshot.paramMap.get('clave');
    this.productoService.buscarProductos(miClave).subscribe(
      (data) =>{
        this.productos = data;
      } );

  }

}
