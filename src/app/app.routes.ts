import { RouterModule, Routes } from '@angular/router';
import { ProductosComponent } from './componentes/productos/productos.component';

const APP_ROUTES: Routes = [
  { path: 'buscar/:clave', component: ProductosComponent },
  { path: 'categoria/:id', component: ProductosComponent },
  { path: 'categoria', component: ProductosComponent },
  {path: 'productos', component: ProductosComponent},
  {path: '', redirectTo: '/productos', pathMatch: 'full'},
  {path: '**', redirectTo: '/productos', pathMatch: 'full'}
  
]

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);


