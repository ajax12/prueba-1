export class Producto {
  sku:String;
  nombre:string;
  descripcion:string;
  precioUnitario:number;
  imagenUrl:string;
  active:boolean;
  unidadEnStock:number;
  fechaCreacion:Date;
  ultimaActualizacion:Date;
}

