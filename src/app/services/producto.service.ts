import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../mis-clases/producto';
import { map } from 'rxjs/operators';
import { Categoria } from '../mis-clases/categoria';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {
  
  private baseUrl = 'http://localhost:8080/api/productos';
  private baseUrlCategorias = 'http://localhost:8080/api/categorias';
  
  
  
  constructor(private http: HttpClient) { }

  getListarProductos(idCategoria: number): Observable<Producto[]> {
    const searchURL = `${this.baseUrl}/search/findByCategoriaId?id=${idCategoria}`;
    return this.getProductos(searchURL);
    }
    
    getProductosPorCategoria():Observable<Categoria[]> {

      return this.http.get<RespuestaProductosCategoria>(this.baseUrlCategorias).pipe(
        map((response) => response._embedded.categorias));

      
    }

    buscarProductos(miClave: string):Observable<Producto[]> {
      const searchURL = `${this.baseUrl}/search/findByNombreContaining?nombre=${miClave}`;
    return this.getProductos(searchURL);
    }

      

  private getProductos(searchURL: string): Observable<Producto[]> {
    return this.http.get<RespuestaProductos>(searchURL).pipe(
      map((response) => response._embedded.productos));
  }
    
    

}

interface RespuestaProductos {
  _embedded: {
    productos: Producto[];
  };
}

interface RespuestaProductosCategoria {
  _embedded: {
    categorias: Categoria[];
  };
}

